﻿
//using CS481_HW6;
 
//var dictionary = Dictionary.FromJson(jsonString);

namespace QuickType
//namespace CS481_HW6
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;
    using QuickType;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Net.Http;
    using System.Runtime.CompilerServices;
    using System.Net.Http.Headers;

    //Here's the json-turned-c# code 
    //Here are the get and sets for greeting and instruction
    public partial class Dictionary
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("definition")]
        public string Definition { get; set; }

        [JsonProperty("pronunciation")]
        public string Pronunciation { get; set; }

    }
    /*
    public partial class Definition
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("definition")]
        public string DefinitionDefinition { get; set; }

    }
    
    //This is where the dictionary deserializes the Owlbot json api
    public partial class Dictionary
    {
        public static Dictionary FromJson(string json) => JsonConvert.DeserializeObject<Dictionary>(json,QuickType.Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this Dictionary self) => JsonConvert.SerializeObject(self, QuickType.Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
    */
}
