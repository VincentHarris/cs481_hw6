﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_HW6
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new MainPage();
            //This references https://github.com/xamarin/xamarin-forms-samples/tree/master/XAML/ResourceDictionaries/ResourceDictionaryDemo
            //This throws an error Xamarin.Forms.Xaml.XamlParseException: 'Position 9:22. StaticResource not found for key PageMargin' in HomePag.xaml.g.cs
            //this uses the xaml/cs of listPage,HomePag, uses ResourceDictionary.xaml
            //MainPage = new NavigationPage(new HomePag()); 
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
