﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Newtonsoft.Json;
using Xamarin.Essentials;
using Newtonsoft.Json;
using System.Net.Http;
using QuickType;

namespace CS481_HW6
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage : ContentPage
    {
        /*
        //references https://www.youtube.com/watch?v=hAmcxAmbLcA
        //This is list that has dummy words that can be searched in the searchbar
        private readonly List<string> somewords = new List<string>
        {
            "Car", "Dog", "Cat", "Jump"
        };
        */
        public MainPage()
        {
            InitializeComponent();
            //This takes the entries in the list named somewords
           //MainListView.ItemsSource = somewords;
        }

        //This throws an error if the word wasn't found or the user entered nothing into the searchbar
        private void WordError(object sender, EventArgs e)
        {
            //Takes user entry and inserts it into Word
            string Wordtext = EnterWord.Text;
            if (string.IsNullOrWhiteSpace(Wordtext))
            {
                DisplayAlert("No words entered/found", "Word was not found in the dictionary", "OK");
            }
        }

        //referenced from http://zetcode.com/csharp/httpclient/
        async void GetWordInfo(string Wordtext)
        {
            HttpClient client = new HttpClient();
            var uri = new Uri(string.Format($"https://owlbot.info/api/v4/dictionary/handsaw")) + Wordtext;

            List<Dictionary> wordData = new List<Dictionary>();
            var response = await client.GetAsync(uri);

            //will deserialize the json content which is the owl api if the status code is 200, which is successful
            if (response.IsSuccessStatusCode)
            {
                var jsonContent = await response.Content.ReadAsStringAsync();
                wordData = JsonConvert.DeserializeObject<List<Dictionary>>(jsonContent);
            }

            WordDataView.ItemsSource = wordData;
        }

        /*
          private void MainSearchBar_OnSearchButtonPressed(object sender, EventArgs e)
          {//The word the user types in the searchbar is the keyword and it searches the list and outputs the key if found
              string keyword = MainSearchBar.Text;
              IEnumerable<string> searchResult = somewords.Where(words => words.Contains(keyword));
              MainListView.ItemsSource = searchResult;
          }
          */
    }
}