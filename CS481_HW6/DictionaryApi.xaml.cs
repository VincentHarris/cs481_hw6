﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
//using QuickType;

using Newtonsoft.Json;

using System.ComponentModel;
using System.Diagnostics;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Net.Http.Headers;

namespace CS481_HW6
{
    //references https://www.youtube.com/watch?v=XQhopjXjSG0
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DictionaryApi : ContentPage
    {
        /*
        private List<readwords> otherwords { get; set; }
        public List<readwords> OtherWords
        {
            get
            { return otherwords;}

            set
            {
                if (value != otherwords)
                { otherwords = value; }
            }
        }
        */
        public DictionaryApi()
        {
           // InitializeComponent();
           // BindingContext = new readwords();
        }
        public async void Button_Clicked(object sender, EventArgs e)
        {

            //This builds and sends to the server
            HttpClient client = new HttpClient();
            var uri = new Uri(string.Format( $"https://owlbot.info/api/v4/dictionary/handsaw"));

            var request = new HttpRequestMessage();
            request.Method = HttpMethod.Get;
            request.RequestUri = uri;

            request.Headers.Add("Application", "application / json");
            client.DefaultRequestHeaders.Authorization =
            new AuthenticationHeaderValue("Token", "AIzaSyCKiTFpZ9-mjVzx4GQ-7gRQ9dPWIM2Vp6c");//api key
            HttpResponseMessage response = await client.SendAsync(request);
            //string postData = null;

            //Reads the content in the response if the http status code is 200 
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
               // postData = readwords.FromJson(content);
                Debug.WriteLine("SUCCESSFUL!");
            }
            else
            {Debug.WriteLine("An error occured while loading data");}
        }

    }
}