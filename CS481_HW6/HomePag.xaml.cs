﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_HW6
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HomePag : ContentPage
    {
        public HomePag()
        {
            InitializeComponent();
        }
        async void OnNavigateButtonClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new ListPage());
        }
    }
}